#!/usr/bin/env python

from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:
    from math import pi, fabs, cos, sqrt
    #defines tau for later use
    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# class name inherited from tutorial
class MoveGroupPythonInterfaceTutorial(object):
    """MoveGroupPythonInterfaceTutorial"""
    #intializes the robot and variables used later in the code
    def __init__(self):
        super(MoveGroupPythonInterfaceTutorial, self).__init__()

       
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()

        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names
	
    #first state of the robot, move it from laying on the plane to straight into the air
    def up_state(self): 
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = -tau/4
        joint_goal[2] = 0
        joint_goal[3] = -tau/4
        joint_goal[4] = 0
        joint_goal[5] = 0 
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)	
        
    #part 2 of the move to the start position of the tracing. This function uses joint_goal to move to a position closer to the angle I want
    def init_pointer1(self): 
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -0.306*tau
        joint_goal[1] = -0.261*tau
        joint_goal[2] = -0.083*tau
        joint_goal[3] = 0.844*tau
        joint_goal[4] = -0.694*tau
        joint_goal[5] = 0 
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)
    # part 3: this function move the robot to it's final position where the end effector in gazebo is facing the camera
    def init_pointer2(self): 
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -0.661*tau
        joint_goal[1] = -0.489*tau
        joint_goal[2] = 0.275*tau
        joint_goal[3] = 0.714*tau
        joint_goal[4] = 0.661*tau
        joint_goal[5] = 0 
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    #In the 6 point rectangle, this pose is the bottom left pose 
    def bottom_left(self): 
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -0.636*tau
        joint_goal[1] = -0.461*tau
        joint_goal[2] = 0.317*tau
        joint_goal[3] = 0.644*tau
        joint_goal[4] = 0.261*tau
        joint_goal[5] = 0 
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)	

	#In the 6 point rectangle, this pose is the top left pose, directly above the mid left point
    def top_left(self):

        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -0.636*tau
        joint_goal[1] = -0.328*tau
        joint_goal[2] = 0.089*tau
        joint_goal[3] = 0.739*tau
        joint_goal[4] = 0.261*tau
        joint_goal[5] = 0 
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    #In the 6 point rectangle, this function move the robot between the top and bottom left points
    def mid_left(self):

        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -0.636*tau
        joint_goal[1] = -0.408*tau
        joint_goal[2] = 0.236*tau
        joint_goal[3] = 0.672*tau
        joint_goal[4] = 0.261*tau
        joint_goal[5] = 0 
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    #This function move the end effector to the right, defining the right side of the rectangle 
    def mid_right(self):

        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -0.427*tau
        joint_goal[1] = -0.411*tau
        joint_goal[2] = 0.233*tau
        joint_goal[3] = 0.678*tau
        joint_goal[4] = 0.053*tau
        joint_goal[5] = 0 
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    #This function moves the end effector to below the mid right point 
    def bottom_right(self):

        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -0.422*tau
        joint_goal[1] = -0.467*tau
        joint_goal[2] = 0.311*tau
        joint_goal[3] = 0.656*tau
        joint_goal[4] = 0.047*tau
        joint_goal[5] = 0 
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    #This function move the end effector to the top right corner in the rectangle
    def top_right(self):

        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -0.422*tau
        joint_goal[1] = -0.328*tau
        joint_goal[2] = 0.067*tau
        joint_goal[3] = 0.761*tau
        joint_goal[4] = 0.042*tau
        joint_goal[5] = 0 
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    #This function puts the end effector between top right and top left and was necessary for creating the letter A
    def mid_top(self):

        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -0.478*tau
        joint_goal[1] = -0.325*tau
        joint_goal[2] = 0.1*tau
        joint_goal[3] = 0.725*tau
        joint_goal[4] = 0.102*tau
        joint_goal[5] = 0 
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    #This function move the end effector to the further point in the letter B, comprising the curve. It falls between Top right and mid right 
    # in the  z axis
    def b_curve1(self):

        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -0.425*tau
        joint_goal[1] = -0.372*tau
        joint_goal[2] = 0.1*tau
        joint_goal[3] = 0.770*tau
        joint_goal[4] = 0.047*tau
        joint_goal[5] = 0 
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)
    #This function move the end effector to the further point in the letter B, comprising the curve. It falls between mid right and bottom right 
    # in the  z axis
    def b_curve2(self):

        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = -0.425*tau
        joint_goal[1] = -0.489*tau
        joint_goal[2] = 0.269*tau
        joint_goal[3] = 0.770*tau
        joint_goal[4] = 0.047*tau
        joint_goal[5] = 0 
        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)


#main function, runs the tracing, guides the user through the different letters and calls specific positions to create each letter
    
def main():
    try:
        print("")
        print("----------------------------------------------------------")
        input(
            "============ Press `Enter` to begin the tutorial by setting up the moveit_commander ..."
        )
        tutorial = MoveGroupPythonInterfaceTutorial()

        input(
            "============ Press `Enter` to move end effector to start position."
        )
        tutorial.up_state()
        tutorial.init_pointer1()
        tutorial.init_pointer2()
        tutorial.bottom_left()

        input(
            "============ Press `Enter` to trace E."
        )

        tutorial.bottom_right()
        tutorial.bottom_left()
        tutorial.mid_left()
        tutorial.mid_right()
        tutorial.mid_left()
        tutorial.top_left()
        tutorial.top_right()
        input(
            "============ Press `Enter` to trace A."
        )

        tutorial.bottom_left()
        tutorial.mid_top()
        tutorial.bottom_right()
        tutorial.mid_left()
        tutorial.mid_right()
        input(
            "============ Press `Enter` to trace B."
        )
        tutorial.top_left()
        tutorial.top_right()
        tutorial.b_curve1()
        tutorial.mid_right()
        tutorial.mid_left()
        tutorial.mid_right()
        tutorial.b_curve2()
        tutorial.bottom_right()
        tutorial.bottom_left()
        tutorial.top_left()

        print("============ Initials Now Complete :)")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()

